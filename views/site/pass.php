<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Zmiana hasła';
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'password_confirm')->passwordInput() ?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Zmień hasło', ['class' => 'btn btn-primary', 'name' => 'change-pass-button']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

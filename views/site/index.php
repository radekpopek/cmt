<?php

/* @var $this yii\web\View */
use yii\helpers\html;

$this->title = 'My Yii Application';
?>
<?php if(!empty($users)){ ?>
    <table class="table table-striped">
        <tr>
            <td>Imię</td>
            <td>Nazwisko</td>
            <td>Email</td>
        </tr>
        <?php foreach ($users as $user){ ?>
            <tr>
                <td>
                    <?= Html::encode("{$user->name}") ?>
                </td>
                <td>
                    <?= Html::encode("{$user->surname}") ?>
                </td>
                <td>
                    <?= Html::encode("{$user->email}") ?>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php } else { ?>
    <div class="text-danger">Lista użytkowników jest pusta</div>
<?php } ?>

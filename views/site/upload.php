<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype'=> 'multipart/form-data']]) ?>

    <?= $form->field($model, 'xlsFile')->fileInput() ?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Zapisz plik', ['class' => 'btn btn-primary', 'name' => 'upload-file-button']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
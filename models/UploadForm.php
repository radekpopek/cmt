<?php

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    public $xlsFile;

    public function rules()
    {
        return[
            [['xlsFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls'],
        ];
    }

    public function upload()
    {
        if($this -> validate()){
            $this->xlsFile->saveAs('uploads/'. $this->xlsFile->baseName . '.'.$this->xlsFile->extension);

            return $this->xlsFile;
        }
        else{
            return false;
        }
    }
}
<?php

namespace app\models;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use app\models\User;

class ChangePasswordForm extends Model
{
    public $id;
    public $password;
    public $password_confirm;

    private $_user;

    public function __construct($id, $config = [])
    {
        $this->_user = User::findIdentity($id);

        if(!$this->_user){
            throw new InvalidParamException("Nie można znaleźć użytkownika");
        }
        $this->id = $this->_user->getId();
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['password', 'password_confirm'], 'required', 'message' => 'Hasło nie może być puste'],
            [['password', 'password_confirm'], 'string', 'min' => 8, 'message' => 'Hasło musi zawierać przynajmniej 8 znaków'],
            ['password_confirm','compare','compareAttribute' => 'password', 'message' => 'Hasła podane w oby polach muszą być takie same'],
        ];
    }

    public function changePassword()
    {
        $user = $this->_user;
        $user->password = yii::$app->getSecurity()->generatePasswordHash($this->password);
        $user->setLastReset(date('Y-m-d'));
        return $user->save(false);
    }
}
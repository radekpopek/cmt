CREATE TABLE yii.user
(
    id int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    surname varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    password varchar(100) NOT NULL,
    auth_key varchar(100) NOT NULL,
    last_reset date NOT NULL
);
INSERT INTO yii.user (name, surname, email, password, auth_key, last_reset) VALUES ('Radosław', 'Popek', 'radoslaw.popek.88@gmail.com', '12478e7ad0e39aa9c35be4b9a694ba9b', 'S8B9-4tW0uZcFxStvP7Uv2JxmG_TaXle', '2017-09-25');
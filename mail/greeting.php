<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<h2>Witaj <?= Html::encode($name.' '.$surname) ?></h2>
<p>W naszym systemie zostało założone konto dla Ciebie, poniżej znajdują się dane do logowania:</p>
<p>Login: </p><?= Html::encode($email) ?>
<p> Hasło: </p><?= Html::encode($password) ?>

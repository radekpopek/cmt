<?php

namespace app\controllers;

use app\models\ChangePasswordForm;
use app\models\PassForm;
use app\models\UploadForm;
use app\models\User;
use app\models\EntryForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $diff = date_diff( new \DateTime(), new \DateTime($model->getUser()->getLastReset()));
            if( $diff->format('%a') >= yii::$app->params['passResetTime']){
                return $this->redirect('site/change-password');
            }
            return $this->goBack();
        }

        if (Yii::$app->user->isGuest) {
            return $this->render('login', ['model' => $model]);
        }

        $query = User::find();

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);
        $users = $query->orderBy('email')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index',[
            'users' => $users,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionChangePassword()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect('index');
        }
        $id = Yii::$app->user->id;

        $model = new ChangePasswordForm($id);

        if($model->load(Yii::$app->request->post()) && $model->validate() && $model->changePassword() ){
            Yii::$app->session->setFlash('success',"Hasło zostało zmienione");
            return $this->redirect('index');
        }

        return $this->render('pass',[
            'model' => $model,
        ]);
    }
    public function actionUploadUsers()
    {
        if (Yii::$app->user->isGuest){
            return $this->redirect('index');
        }

        $model = new UploadForm();

        if (yii::$app->request->isPost){
            $model->xlsFile = UploadedFile::getInstance($model, 'xlsFile');
            $file = $model->upload();
            if($file){
                $excel = \PHPExcel_IOFactory::load('uploads/'.$file);
                $excelArray = $excel->getActiveSheet()->toArray(null, true, true , true);
                $messages = [];
                $i = 0;                                                     //licznik ilosci dodanych uzytkownikow
                foreach($excelArray as $row) {
                    $randomPassword = yii::$app->security->generateRandomString(8);
                    $hash = yii::$app->security->generatePasswordHash($randomPassword);
                    $user = new User();
                    $user->name = $row['A'];
                    $user->surname = $row['B'];
                    $user->email = $row['C'];
                    $user->password = $hash;

                    $user->last_reset = date('Y-m-d');
                    if ($user->save()) {
                        $messages[] = yii::$app->mailer->compose('greeting.php', [
                            'name' => $user->name,
                            'surname' => $user->surname,
                            'password' => $randomPassword,
                            'email' => $user->email,
                        ])
                            ->setFrom(yii::$app->params['adminEmail'])
                            ->setTo($user->email)
                            ->setSubject('Witaj w naszej aplikacji');
                        $i++;
                    }
                }
                if(!empty($messages)){
                    yii::$app->mailer->sendMultiple($messages);
                    yii::$app->session->setFlash('success','Dodano '.$i.' użytkowników');
                }                    else{
                    yii::$app->session->setFlash('danger','W pliku nie znaleziono poprawnych adresów email');
                }
                unlink('uploads/'.$file);
                //return $this->redirect('index');
                }
        }
        return $this->render('upload',[
           'model' => $model,
        ]);
    }
}
